import ControlsContainer from 'containers/ControlsContainer';
import RouteForm from 'components/RouteForm';
import RouteDetails from 'components/RouteDetails';

export default [{
  path: '',
  component: ControlsContainer,
  children: [{
    path: '',
    component: RouteForm,
  }, {
    path: 'route/:id',
    component: RouteDetails,
    meta: { layout: 'routeDetails' },
  }],
}];
