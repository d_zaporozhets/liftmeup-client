import { mapGetters, mapActions } from 'vuex';

export default {
  name: 'controls-container',
  methods: {
    ...mapActions('controls/routeControl', [
      'updateFormModel',
    ]),
  },
  computed: {
    ...mapGetters('controls/routeControl', [
      'newRoutePoints',
    ]),
  },
};
