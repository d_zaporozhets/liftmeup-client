import Notification from 'components/Notification';
import NotificationAction from 'components/Notification/NotificationAction';
import { mapGetters, mapActions } from 'vuex';

export default {
  name: 'notification-container',
  components: {
    Notification,
    NotificationAction,
  },
  computed: {
    ...mapGetters('notifications', ['notifications']),
    toDisplay() {
      return this.notifications.slice(0, 3);
    }
  },
  methods: {
    ...mapActions('notifications', ['remove'])
  }
};
