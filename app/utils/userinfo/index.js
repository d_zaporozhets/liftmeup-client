import cookie from 'cookie';
import config from 'config';

function getCookie(key) {
  return cookie.parse(document.cookie)[key];
}

const userinfo = config.env === 'production' ? {
  email: getCookie('OauthEmail'),
  picture: getCookie('OauthPicture'),
  token: getCookie('OauthAccessToken')
} : config.devUser;

export default userinfo;
