import apiLoader from './api_loader';

class MapController {
  constructor(api = null) {
    this._api = api;
  }

  // Api section
  setApi(instance) {
    this._api = instance;
    return this._api;
  }

  getApi() {
    return this._api;
  }

  isApiAvailible() {
    return Boolean(this._api);
  }

  loadApi(options = {}) {
    return apiLoader(options).then(instance => {
      this._api = instance;
      return instance;
    });
  }

  // Map
  createMap(container, state, options) {
    this._map = new (this._api).Map(container, state, options);
    return this;
  }

  addMapEvent(event, callback) {
    this._map.events.add(event, callback);
  }

  // Routes
  createRoute({ pointA, pointB, transitPoints }) {
    return new (this._api).multiRouter.MultiRoute({
      referencePoints: [pointA, transitPoints, pointB],
      params: {
        boundedBy: [[55.151361, 73.045407], [54.807785, 73.855648]],
        results: 1,
      }
    }, {
      boundsAutoApply: true
    });
  }

  replaceCollection(collection) {
    const mapCollection = this._map.geoObjects;

    mapCollection.removeAll();
    mapCollection.add(collection);
  }

  clearMap(){
    this._map.geoObjects.removeAll();
  }

  addToMap(collection) {
    this._map.geoObjects.add(collection);
  }

  async geocode(param, options) {
    return this._api.geocode(param, options);
  }
}

export default new MapController();
