import sio from 'socket.io-client';
import config from 'config';
import userinfo from '../userinfo';

const serverUri = config.socket.serverUri;
const { token, email } = userinfo;

export function path(path) {
  return sio(`${serverUri}${path}`, { query: { token, email }});
}

export function getRequest(evt, data) {
  this.emit(evt, data);
  return new Promise(resolve => {
    this.on(`${evt}-resp`, respData => resolve(respData));
  });
}
