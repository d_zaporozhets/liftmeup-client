import { path as socketPath, getRequest } from 'sockets';

const socket = socketPath('/routes');
const request = getRequest.bind(socket);

export async function get() {
  return request('get');
}

export function save(route) {
  return request('save', route);
}

export function getById(id) {
  return request('getById', id);
}
