import { path as socketPath } from 'sockets';
import store from 'store';

const socket = socketPath('/notifications');

socket.on('notify', notification => {
  store.dispatch('notifications/collect', notification);
});
