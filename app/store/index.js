import Vue from 'vue';
import Vuex from 'vuex';
import ymaps from './modules/ymaps';
import user from './modules/user';
import notifications from './modules/notifications';
import routes from './modules/routes';
import controls from './modules/controls';
import createLogger from 'vuex/dist/logger';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

// initial state
const state = {
};

// getters
const getters = {
};

// actions
const actions = {
  onLoad({ commit, getters, dispatch }) {
  }
};

// mutations
const mutations = {
};

export default new Vuex.Store({
  modules: {
    controls,
    notifications,
    user,
    routes,
    ymaps
  },
  state,
  getters,
  mutations,
  actions,
  strict: debug,
  plugins: debug ? [createLogger()] : []
});
