export const YMAP_START_INIT                            = 'YMAP_START_INIT';
export const YMAP_MAP_CREATED                           = 'YMAP_MAP_CREATED';
export const YMAP_API_LOADING                           = 'YMAP_API_LOADING';
export const YMAP_API_LOADED                            = 'YMAP_API_LOADED';
export const YMAP_API_FAILED                            = 'YMAP_API_FAILED';
export const YMAP_API_ADD_ROUTE                         = 'YMAP_API_ADD_ROUTE';
export const YMAP_SET_LAYOUT                            = 'YMAP_SET_LAYOUT';

// Controls

// Routes
export const ROUTES_SAVE_ROUTE   = 'ROUTES_SAVE_ROUTE';
export const ROUTES_LOAD = 'ROUTES_LOAD';
export const ROUTES_SET_ROUTE = 'ROUTES_SET_ROUTE';

// Notifications
export const COLLETCT_NOTIFICATIONS = 'COLLETCT_NOTIFICATIONS';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';
