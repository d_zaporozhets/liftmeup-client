import userInfo from '../../utils/userinfo';

const state = {
  ...userInfo
};

const getters = {
  userEmail: state => state.email,
  userPicture: state => state.picture
};

const actions = {
};

// mutations
const mutations = {
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
