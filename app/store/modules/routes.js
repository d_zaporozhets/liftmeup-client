import * as routes from 'sockets/routes';
import * as types from '../mutation-types';

const state = {
  userRoutes: [],
  routes: {}
};

const getters = {
  routeById: ({ routes }) => id => routes[id],
};

const actions = {
  async saveRoute({ commit }, route) {
    const { points, auto } = route;

    commit(types.ROUTES_SAVE_ROUTE, await routes.save({ ...points, auto }));
  },
  async loadRoutes({ commit }) {
    commit(types.ROUTES_LOAD, await routes.get());
  },
  async getRouteById({ commit, getters: { routeById }}, id) {
    let route = routeById(id);

    if (!route) {
      route = await routes.getById(id);
      commit(types.ROUTES_SET_ROUTE, { id, route });
    }

    return route;
  }
};

// mutations
const mutations = {
  [types.ROUTES_SAVE_ROUTE](state, route) {
    state.userRoutes.push(route);
  },
  [types.ROUTES_SET_ROUTE](state, { id, route } ) {
    state.routes[id] = route;
  },
  [types.ROUTES_LOAD](state, routes) {
    state.userRoutes = routes;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
