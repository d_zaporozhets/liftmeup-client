// import * as types from '../../mutation-types';
import routeControl from './route_control';

// initial state
const state = {
};

// getters
const getters = {
  route: state => state.routeControl.route
};

// actions
const actions = {
};

// mutations
const mutations = {
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: { routeControl }
};
