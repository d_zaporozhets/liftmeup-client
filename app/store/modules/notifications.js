import 'sockets/notifications';
import * as types from '../mutation-types';

const state = {
  notifications: [{
    body: 'testtet',
    actions: [{
      name: 'TEst',
      payload: {
        path: '/route/59e2e7f388c77c76503a9848'
      },
    }],
  }],
};

const getters = {
  notifications: state => state.notifications
};

const actions = {
  collect({ commit }, notification) {
    commit(types.COLLETCT_NOTIFICATIONS, notification);
  },
  remove({ commit }, id) {
    commit(types.REMOVE_NOTIFICATION, id);
  }
};

// mutations
const mutations = {
  [types.COLLETCT_NOTIFICATIONS](state, notification) {
    state.notifications.push(notification);
  },
  [types.REMOVE_NOTIFICATION](state, id) {
    state.notifications.splice(id, 1);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
