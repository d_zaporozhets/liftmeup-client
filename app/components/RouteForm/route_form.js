import ButtonMenu from 'components/ButtonMenu';
import ymaps from 'ymaps';
import { mapActions } from 'vuex';

export default {
  name: 'route-form',
  components: {
    ButtonMenu
  },
  data() {
    return {
      isOpen: false,
      city: 'Омск',
      route: {
        points: {
          pointA: '',
          pointB: '',
          transitPoints: []
        },
        auto: {
          number: '',
          seats: 0
        },
      },
    };
  },
  computed: {
    pointA: {
      set(value) { this.setPoint('pointA', value) },
      get() { return this.getPoint('pointA') }
    },
    pointB: {
      set(value) { this.setPoint('pointB', value) },
      get() { return this.getPoint('pointB') }
    }
  },
  methods: {
    setPoint(point, value) {
      this.route.points[point] = `${this.city}, ${value}`;
    },
    getPoint(point) {
      return this.route.points[point].replace(`${this.city}, `, '');
    },
    onOpen() {
      this.isOpen = true;
      this.$emit('open');
    },
    onClose() {
      this.isOpen = false;
      this.$emit('close');
    },
    addRouteToMap() {
      this.route.routeObject = ymaps.createRoute(this.route.points);
      ymaps.addToMap(this.route.routeObject);
    },
    addTransitPoint(index) {
      const point = this.route.points.transitPoints[index].value;

      if (point) {
        const refPoints = this.route.routeObject.model.getReferencePoints();
        const viaPointIndexes = this.route.routeObject.model.getReferencePointIndexes().via;

        refPoints.splice(index + 1, 0, point);
        viaPointIndexes.push(viaPointIndexes.length + 1);
        this.route.routeObject.model.setReferencePoints(refPoints, viaPointIndexes);

        this.route.points.transitPoints[index].added = true;
      }
    },
    removeTransitPoint(index) {
      if (this.route.points.transitPoints[index].added) {
        const refPoints = this.route.routeObject.model.getReferencePoints();
        const viaPointIndexes = this.route.routeObject.model.getReferencePointIndexes().via;

        refPoints.splice(index + 1, 1);
        viaPointIndexes.pop();
        this.route.routeObject.model.setReferencePoints(refPoints, viaPointIndexes);
      }
      this.route.points.transitPoints.splice(index, 1);
    },
    ...mapActions('routes', ['saveRoute'])
  }
};
