export default {
  name: 'notification',
  props: {
    content: Object
  },
  data() {
    return {
      browserNotifications: false
    };
  },
  computed: {
  },
  methods: {
    notify({ title, body }) {
      const { browserNotifications } = this;

      if (browserNotifications) new Notification(title, { body });
    }
  },
  async created() {
    // if ('Notification' in window) {
    //   const permission = await Notification.requestPermission();
    //   this.browserNotifications = permission === 'granted';
    // }
    // const notification = socketPath('/notifications');
    // notification.on('notify', this.notify);
  },
  updated() {
  }
};
