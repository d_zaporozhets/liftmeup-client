export default {
  name: 'button-menu',
  data: () => {
    return {
      classes: {
        'button-menu--collapsed': true
      }
    };
  },
  methods: {
    toogle() { this.classes['button-menu--collapsed'] ? this.open() : this.close() },
    close() {
      this.classes['button-menu--collapsed'] = true;
      this.$emit('close');
    },
    open() {
      this.classes['button-menu--collapsed'] = false;
      this.$emit('open');
    }
  }
};
