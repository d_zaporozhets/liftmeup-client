export default {
  name: 'button-back',
  mounted() {
  },
  methods: {
    goBack() {
      this.$router.go(-1);
    }
  }
};
