module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: [
    'standard',
    'eslint:recommended',
    'plugin:vue/recommended',
  ],
  // add your custom rules here
  'rules': {
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    //
    'no-extra-semi': 1,
    //
    // 'space-before-function': 0,
    'space-before-function-paren': ['error', 'never'],
    //
    'semi': ['error', 'always', { 'omitLastInOneLineBlock': true }],
    //
    'semi-spacing': ['error', { 'before': false, 'after': true}],
    //
    'no-multi-spaces': ['error', { 'exceptions': { 'ImportDeclaration': true, 'VariableDeclarator': true } }],
    //
    'no-new': 0,
    //
    'object-curly-spacing': ['error', 'always', {
      'objectsInObjects': false,
      'arraysInObjects': false
    }],
    'standard/object-curly-even-spacing': ['error', 'always', {
      'objectsInObjects': false,
      'arraysInObjects': false
    }],
  },
  globals: {
    'rm': true,
    'mkdir': true,
    'cp': true,
    'env': true,
    'ymaps': true,
    'fetch': true,
    'Notification': true
  }
}
