import * as utils from './utils';
import config from './env';
const isProduction = process.env.NODE_ENV === 'production';

export default {
  loaders: utils.cssLoaders({
    sourceMap: isProduction
      ? config.build.productionSourceMap
      : config.dev.cssSourceMap,
    extract: isProduction
  })
};
