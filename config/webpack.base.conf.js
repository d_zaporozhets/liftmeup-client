import eslintFormatter from 'eslint-friendly-formatter';
import config          from './env';
import * as utils      from './utils';
import vueLoaderConfig from './vue-loader.conf';

export default {
  entry: {
    app: utils.resolveAppSrc('main.js'),
  },
  output: {
    path: config.build.assetsRoot,
    publicPath: process.env.NODE_ENV === 'production' ? config.build.assetsPublicPath : config.dev.assetsPublicPath,
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      vue$: 'vue/dist/vue.esm.js',
      '@': utils.resolve('app'),
      assets: utils.resolveAppSrc('assets'),
      components: utils.resolveAppSrc('components'),
      containers: utils.resolveAppSrc('containers'),
      store: utils.resolveAppSrc('store'),
      ymaps: utils.resolveAppSrc('utils/ymaps/controller'),
      config: utils.resolveAppSrc('config'),
      sockets: utils.resolveAppSrc('utils/sockets'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [utils.resolve('app'), utils.resolve('test')],
        options: {
          formatter: eslintFormatter,
        },
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig,
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          utils.resolve('app'),
          utils.resolve('test'),
          utils.resolve('node_modules/vuex'),
        ],
        exclude: [
          /node_modules(?![\\/]vuex[\\/])/,
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('img/[name].[hash:7].[ext]'),
        },
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('fonts/[name].[hash:7].[ext]'),
        },
      },
    ],
  },
};
