import ButtonMenu from 'components/ButtonMenu';

describe('ButtonMenu.vue', () => {
  it('should open on click', () => {
    const defaultData = ButtonMenu.data();
    expect(defaultData.classes['button-menu--collapsed']).to.be.equal(true);
  });
});
