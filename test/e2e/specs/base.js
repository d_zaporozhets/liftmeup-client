export function load(client) {
  const devServer = client.globals.devServerURL;

  client.url(devServer);
  return devServer;
}

export default {
  'On load': function test(client) {
    load(client);

    client.expect.element('#app').to.be.present.before(1000);

    client.end();
  }
};
