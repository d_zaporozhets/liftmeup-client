import path                 from 'path';
import express              from 'express';
import webpack              from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import historyApiFallback   from 'connect-history-api-fallback';
import webpackConfigProd    from '../config/webpack.prod.conf';
import webpackConfigDev     from '../config/webpack.dev.conf';
import checkVersions        from './check-versions';
import config               from '../config/env';
// import opn                  from 'opn';

checkVersions();

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV);
}

const webpackConfig = process.env.NODE_ENV === 'testing' ? webpackConfigProd : webpackConfigDev;

const port = process.env.PORT || config.dev.port;

const app = express();
const compiler = webpack(webpackConfig);

const devMiddleware = webpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  quiet: true
});

const hotMiddleware = webpackHotMiddleware(compiler, {
  log: () => {}
});

compiler.plugin('compilation', (compilation) => {
  compilation.plugin('html-webpack-plugin-after-emit', (data, cb) => {
    hotMiddleware.publish({ action: 'reload' });
    cb();
  });
});

// handle fallback for HTML5 history API
app.use(historyApiFallback());

// serve webpack bundle output
app.use(devMiddleware);

// enable hot-reload and state-preserving
// compilation error display
app.use(hotMiddleware);

// serve pure static assets
const staticPath = path.posix.join(config.dev.assetsPublicPath, config.dev.assetsSubDirectory);
app.use(staticPath, express.static('./static'));

const uri = `http://localhost:${port}`;

let _resolve;
const readyPromise = new Promise(resolve => {
  _resolve = resolve;
});

devMiddleware.waitUntilValid(() => {
  console.log('> Listening at ' + uri + '\n'); // eslint-disable-line
  _resolve();
});

const server = app.listen(port);

export default {
  ready: readyPromise,
  close: () => {
    server.close();
  }
};
